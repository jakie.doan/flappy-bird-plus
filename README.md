# Flappy Bird Plus



## Getting started

 ```
cd existing_repo
git remote add origin https://gitlab.com/jakie.doan/flappy-bird-plus.git
git branch -M main
git push -uf origin main
```

## Name
Flappy Bird Plus.

## Description
Flappy Bird Plus game.

## Run .exe
Open Build folder to run the game .exe file.

# Demo
## Level 1
![image](https://gitlab.com/jakie.doan/flappy-bird-plus/-/raw/main/Image/Level1.png)

## Level 2
![image](https://gitlab.com/jakie.doan/flappy-bird-plus/-/raw/main/Image/Level2.png)

## Level 3
![image](https://gitlab.com/jakie.doan/flappy-bird-plus/-/raw/main/Image/Level3.png)

## Level 4
![image](https://gitlab.com/jakie.doan/flappy-bird-plus/-/raw/main/Image/Level4.png)

## Play screen
![image](https://gitlab.com/jakie.doan/flappy-bird-plus/-/raw/main/Image/demo.gif)