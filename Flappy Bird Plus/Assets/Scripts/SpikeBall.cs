using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBall : MonoBehaviour
{
    private Rigidbody2D rigidbody;

    private float moveSpeed = 20f;
    private float leftAngle = -0.35f;
    private float rightAngle = 0.35f;

    private bool movingClockwise = true;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeSwingDirection();

        if (movingClockwise)
        {
            rigidbody.angularVelocity = moveSpeed;
        }

        if (!movingClockwise)
        {
            rigidbody.angularVelocity = -1 * moveSpeed;
        }
    }

    void ChangeSwingDirection()
    {
        if (transform.rotation.z > rightAngle)
        {
            movingClockwise = false;
        }
        else if (transform.rotation.z < leftAngle)
        {
            movingClockwise = true;
        }
    }
}
