using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private PlayerMovement player;
    private SpriteRenderer sprite;

    public Text scoreText;
    public Text levelText;
    public GameObject playButton;
    public GameObject gameLevel;
    public GameObject nextLevel;
    public GameObject replayLevel;

    private bool isDead;
    public int items { get; private set; }

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();

        int currentLevel = SceneManager.GetActiveScene().buildIndex + 1;
        levelText.text = "Level " + currentLevel.ToString();

        nextLevel.SetActive(false);
        replayLevel.SetActive(false);
        isDead = false;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void Awake()
    {
        Application.targetFrameRate = 60;
        player = FindObjectOfType<PlayerMovement>();
        Pause();
    }

    public void Play()
    {
        if (items == 3)
        {
            LoadNextLevel();
        }
        else if (isDead == true)
        {
            ReloadLevel();
        }

        items = 0;
        scoreText.text = items.ToString();

        playButton.SetActive(false);

        Time.timeScale = 1f;
        player.enabled = true;
    }

    void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

    void ReloadLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void GameOver()
    {
        isDead = true;
        playButton.SetActive(true);
        gameLevel.SetActive(false);
        replayLevel.SetActive(true);

        Pause();
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        player.enabled = false;

        if (items == 3)
        {
            playButton.SetActive(true);
            gameLevel.SetActive(false);
            nextLevel.SetActive(true);
        }
    }

    public void TurnLeft()
    {
        player.spriteRenderer.flipX = true;
        player.turnLeft = true;
    }

    public void TurnRight()
    {
        player.spriteRenderer.flipX = false;
        player.turnLeft = false;
    }


    public void CollectItems(Collider2D collision)
    {
        items++;
        scoreText.text = items.ToString();
        Destroy(collision.gameObject);

        if (items == 3)
        {
            Pause();
        }
    }
}
